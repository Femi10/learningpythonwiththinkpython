
import random
import time
from pprint import pprint


board = ['0', '1', '2', '3', '4', '5', '6', '7', '8']
turn = 'X'

MoveCount = 0
defaultt=''


def printBoard(board):
    print(board[0]+'|'+board[1]+'|'+board[2]+'|')
    print('------')
    print(board[3]+'|'+board[4]+'|'+board[5]+'|')
    print('------')
    print(board[6]+'|'+board[7]+'|'+board[8]+'|')
    print('------')


def chooseOpponent():
    global defaultt
    print('Choose Opponent to play against')
    print('Player:   1')
    print('Computer: 2')
    print('Help:     3')
    
    try:
        playerChoice = int(input())
        if playerChoice == 1:
            defaultt = False
        elif playerChoice==2:
            defaultt = True
        elif playerChoice == 3:
            print('Welcome to TicTacToe game')
            print('Press The NUmbers on the board to play the game')
            print('Match x or 0 in a row , column or diagnol')

    except ValueError:
        print('enter an integer')


def game(board):
    chooseOpponent()
    global defaultt
    if defaultt == True or defaultt ==False:
      printBoard(board)
      playerToStart()
      gameplay(board)


def playerToStart():
    global turn
    global defaultt
    start = random.randint(0, 1)
    if defaultt:
       if start == 0:
        print('player 1 to start the game')
        print('player 1 is X ')
        turn = 'X'
       else:
        print('Computer to start the game')
        print('Computer is O')
        turn = 'O'
    else:
        if start == 0:
         print('player 1 to start the game')
         print('player 1 is X ')
         turn = 'X'
        else:
         print('Player 2 to start the game')
         print('Player is O')
         turn = 'O'



def gameplay(board):

    while True:
        global MoveCount
        global turn

        if turn == '':
            print('Game over')
            return

        if MoveCount == 9:
            print("it's Tie")
            printBoard(board)
            print('Game Over')
            break

        if turn == 'X':
            player1(board)
        if defaultt:
          if turn == 'O':
            print('Computer to select a move')
            print('thinking ....')
            time.sleep(1)
            computerTurn(board)
        else:
          if turn == 'O':
              player2(board)




def checkwin(board):
    global turn
    if board[0] == board[1] == board[2]:
        turn = ''
        return True
    elif board[3] == board[4] == board[5]:
        turn = ''
        return True

    elif board[6] == board[7] == board[8]:
        turn = ''
        return True
    elif board[0] == board[3] == board[6]:
        turn = ''
        return True
    elif board[1] == board[4] == board[7]:
        turn = ''
        return True
    elif board[2] == board[5] == board[8]:
        turn = ''
        return True
    elif board[0] == board[4] == board[8]:
        turn = ''
        return True
    elif board[2] == board[4] == board[6]:
        turn = ''
        return True

    return False


def computerTurn(board):
 global MoveCount, turn
   
    


 move = random.randint(0,8)
 MoveCount += 1
 if isPlayerWining(board):
    blockPlayer(board)


 else:
    
    if cannotMove(board,move):

        print('Slot Already occupied')
        MoveCount -= 1
    
    else:
        board[move] = turn
        com(board)

def com(board):
    global turn
    if checkwin(board):
            print('Computer Wins')
            printBoard(board)
            return
    else:
            printBoard(board)
            turn = 'X'
          
    
def player1(board):
    global MoveCount, turn
    try:
        print('Player 1 to select a move')

        move = int(input())
        MoveCount += 1

        if board[move] == 'X' or board[move] == 'O':
            print('Slot Already occupied')
            MoveCount -= 1

        else:
            board[move] = turn
            if checkwin(board):
                print('player 1 wins')
                printBoard(board)
                return
            else:
                printBoard(board)
                turn = 'O'
    except (IndexError, ValueError):
        print('Please Enter An integer between 0-8')
        MoveCount -= 1


def player2(board):
    global MoveCount, turn
    try:
        print('Player 2 to select a move')

        move = int(input())
        MoveCount += 1

        if cannotMove(board,move):
            print('Slot Already occupied')
            MoveCount -= 1

        else:
            board[move] = turn
            if checkwin(board):
                print('player 2 wins')
                printBoard(board)
                return
            else:
                printBoard(board)
                turn = 'X'
    except (IndexError, ValueError):
        print('Please Enter An integer between 0-8')
        MoveCount -= 1
def cannotMove(board,move=''):
    if board[move] == 'X' or board[move] == 'O':

        return True
    return False
           



def isPlayerWining(board):
    
    
    if board[0] == board[1] or board[0] == board[2] or board[1] == board[2]:
        return True
    elif board[3] == board[4] or board[3] == board[5] or board[4] == board[5]:
        return True
    elif board[6] == board[7] or board[7] == board[8] or board[6] == board[8]:
        return True
    elif board[0] == board[3] or board[0] == board[6] or board[3] == board[6]:
        return True
    elif board[1] == board[4] or board[1] == board[7] or board[4] == board[7]:
        return True
    elif board[2] == board[5] or board[2] == board[8] or board[5] == board[8]:
        return True
    elif board[0] == board[4] or board[0] == board[8] or board[4] == board[8]:
        return True
    elif board[2] == board[4] or board[2] == board[6] or board[4] == board[6]:
        return True

    return False

def blockPlayer(board):
    
    global MoveCount
    
    if board[0] == board[1] or board[0] == board[2] or board[1] == board[2]:
       
        
        move=random.choice([0,1,2])
        if cannotMove(board,move):
            print('Slot Already occupied')
            MoveCount -= 1
        else:
            board[move]=turn 
            com(board)     
        
    elif board[3] == board[4] or board[3] == board[5] or board[4] == board[5]:
        
        move=random.choice([3,4,5])
        if cannotMove(board,move):
            print('Slot Already occupied')
            MoveCount -= 1
        else:
            board[move]=turn   
            com(board)     
           
        

    elif board[6] == board[7] or board[7] == board[8] or board[6] == board[8]:
        
        move=random.choice([6,7,8])
        if cannotMove(board,move):
            print('Slot Already occupied')
            MoveCount -= 1
        else:
            board[move]=turn   
            com(board)     
        
        
    elif board[0] == board[3] or board[0] == board[6] or board[3] == board[6]:
        move=random.choice([0,3,6])
        if cannotMove(board,move):
            print('Slot Already occupied')
            MoveCount -= 1
        else:
            board[move]=turn   
            com(board)     
       
       
        
    elif board[1] == board[4] or board[1] == board[7] or board[4] == board[7]:
        
        move=random.choice([1,4,7])
        if cannotMove(board,move):
            print('Slot Already occupied')
            MoveCount -= 1
        else:
            board[move]=turn   
            com(board)     
        
        
    elif board[2] == board[5] or board[2] == board[8] or board[5] == board[8]:
        
        move=random.choice([2,5,8])
        if cannotMove(board,move):
            print('Slot Already occupied')
            MoveCount -= 1
        else:
            board[move]=turn   
            com(board)     
        
        
        
    elif board[0] == board[4] or board[0] == board[8] or board[4] == board[8]:
        return True
    elif board[2] == board[4] or board[2] == board[6] or board[4] == board[6]:
        return True

    return False
























game(board)


